import {http} from './index'
export function login(data){
    return http("login","post",data)
}
//获取权限列表
export function getMenus(){
    return http('menus','GET')
}
//获取用户列表
export function getUsers(params){
    return http('users','GET',{},params)
}
//添加用户
export function addUsers(data){
    return http('users','POST',data)
}