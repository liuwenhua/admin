import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const routes = [
//   {
//     path:'/',
//     redirect: '/login'
// },
  {
    path:'/',
    name:'login',
    component:() => import(/* webpackChunkName: "login" */ '../views/Login.vue')
  },{
    path:'/index',
    name:'index',
    component:() => import(/* webpackChunkName: "index" */ '../views/index.vue'),
    children:[
      {
        path:"roles",
        name:"roles",
        component:()=>import(/* webpackChunkName: "roles" */ '../views/roles/roles.vue')
      },{
        path:"users",
        name:"users",
        component:()=>import(/* webpackChunkName: "users" */ '../views/users/users.vue')
      },{
        path:"rights",
        name:"rights",
        component:()=>import(/* webpackChunkName: "rights" */ '../views/rights/rights.vue')
      }
    ]
  }
  
]

const router = new VueRouter({
  routes
})

export default router
